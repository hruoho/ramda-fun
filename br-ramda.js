const R = require('ramda')
const data = require('./brilliants.json')
const listMaximum = R.reduce(R.max, 0)
const mem = require('mem')

// reportResults(Object.keys(data).slice(0, 2))
reportResults(Object.keys(data))

function reportResults (keys) {
  console.time('time taken')
  console.log('max total:', R.sum(getMaxValues(keys)))
  console.timeEnd('time taken')
}

function getMaxValues (keys) {
  const getStone = R.flip(R.prop)(data)
  return R.map(R.pipe(getStone, getMaxValue))(keys)
}

function getMaxValue (stone) {
  const getRawChunks = R.prop('rawChunks')
  const cutter = mem(chunk => applyCuts(stone.cuts, chunk, 0))
  return R.pipe(getRawChunks, R.map(cutter), R.sum)(stone)
}

function applyCuts (cuts, chunk, value) {
  const sizeLens = R.lensProp('size')
  const isCutPossible = R.pipe(R.view(sizeLens), R.gte(chunk))
  const applyCut = cut => applyCuts(cuts, chunk - cut.size, value + cut.value)

  return R.pipe(
    R.filter(isCutPossible),
    R.ifElse(R.length,
      R.pipe(R.map(applyCut), listMaximum),
      R.always(R.subtract(value, chunk))
    )
  )(cuts)
}

console.assert(applyCuts(data.diamond.cuts, 23, 0), 27)
console.assert(applyCuts(data.diamond.cuts, data.diamond.rawChunks[0], 0), 23)
