var data = require('./brilliants.json')
const mem = require('mem')

const memCalcMaxValue = mem(calcMaxValue)

console.time('timer')
console.log('total max: ' + getMaxTotal())
console.timeEnd('timer')

function getMaxTotal () {
  var total = 0

  for (var stone in data) {
    total += calcMaxValues(data[stone])
  }

  return total
}

function calcMaxValues (stone) {
  var max = 0

  for (var rawChunk of stone.rawChunks) {
    max += memCalcMaxValue(stone.cuts, rawChunk, 0)
  }

  return max
}

function calcMaxValue (cuts, chunk, value) {
  var max = 0
  for (var cut of cuts) {
    if (cut.size > chunk) continue
    max = Math.max(max, memCalcMaxValue(cuts, chunk - cut.size, value + cut.value))
  }

  return max || value - chunk
}
