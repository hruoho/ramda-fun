/**
  An attempt to solve https://wunder.dog/brilliant-cut.
  Currently inefficient and verbose...
*/
const R = require('ramda')
const data = require('./brilliants.json')

// getResults(Object.keys(data).slice(0, 2))
getResults(Object.keys(data))

function getResults (keys) {
  console.time('time taken')
  const results = getMaxValues(keys)
  console.log('max total:', R.sum(results))
  console.timeEnd('time taken')
}

function getMaxValues (keys) { // max values for all "stones"
  const getStone = R.prop(R.__, data)
  return R.map(R.pipe(getStone, getMaxValue))(keys)
}

function getMaxValue (stone) {
  const getRawChunks = R.prop('rawChunks')
  const makeChunkValues = R.map(chunk => [chunk, 0])
  const findMaxProfits = R.map(chunkValue => findMaxProfit(stone.cuts, [chunkValue], 0))
  return R.pipe(getRawChunks, makeChunkValues, findMaxProfits, R.sum)(stone) // sum of max profits per stone
}

function findMaxProfit (cuts, chunkValues, currentMaxProfit) {
  if (!chunkValues.length) return currentMaxProfit
  const curriedSelf = R.curry(findMaxProfit)

  return R.pipe(
    getCutters(),
    getNewChunkValues(chunkValues),
    R.converge(curriedSelf(cuts), [R.identity, getNewMax(currentMaxProfit)])
  )(cuts) // tail-recursive call to self with "newChunkValues" and "newMaxProfit" ...really?
}

function getCutters () {
  const cutPossible = cut => chunk => chunk >= cut.size // R.lte(R.prop('size', cut), R.__)
  const newChunkValue = cut => (chunk, value) => [chunk - cut.size, value + cut.value]
  const getCutter = cut => R.ifElse(
    cutPossible(cut),
    newChunkValue(cut),
    R.always([])
  )

  return R.map(getCutter)
}

function getNewMax (currentMaxProfit) {
  const listMaximum = R.apply(Math.max)
  const getProfit = ([chunk, value]) => value - chunk
  const getProfits = R.map(getProfit)

  return R.pipe(getProfits, R.append(currentMaxProfit), listMaximum)
}

function getNewChunkValues (chunkValues) {
  const applyCutter = R.pipe(R.apply, R.map(R.__, chunkValues))
  const applyCutters = R.map(applyCutter)
  const normalizeList = R.pipe(R.unnest, R.filter(R.length))

  return R.pipe(applyCutters, normalizeList)
}

console.assert(findMaxProfit(data.diamond.cuts, [[23, 0]], 0) === 27)
