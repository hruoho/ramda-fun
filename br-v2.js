const data = require('./brilliants.json')

console.time('timer')
console.log('total max: ' + getTotalMax(Object.keys(data)))
console.timeEnd('timer')

function getTotalMax (keys) {
  return keys
    .map(getStone)
    .map(cutStone)
    .reduce((acc, cur) => acc + cur)
}

function getStone (key) {
  return [data[key].rawChunks, data[key].cuts]
}

function cutStone ([chunks, cuts]) {
  return chunks.reduce((acc, rawChunk) =>
    acc + calcMaxValue(cuts, rawChunk, 0), 0)
}

function calcMaxValue (cuts, chunk, value) {
  return cuts.reduce((acc, cut) => {
    if (cut.size > chunk) return acc
    return Math.max(acc, calcMaxValue(cuts, chunk - cut.size, value + cut.value))
  }, 0) || value - chunk
}

console.assert(cutStone([[23], data.diamond.cuts]) === 27)
