const data = require('./brilliants.json')

console.time('timer')
console.log('total max: ' + getTotalMax(Object.keys(data)))
console.timeEnd('timer')

function getTotalMax (keys) {
  return keys
    .map(getStone)
    .map(cutStone)
    .reduce((acc, cur) => acc + cur)
}

function getStone (key) {
  return [data[key].rawChunks, data[key].cuts]
}

function cutStone ([chunks, cuts]) {
  return chunks.reduce((acc, rawChunk) =>
    acc + calcMaxValue(cuts, [[rawChunk, 0]]), 0)
}

function calcMaxValue (cuts, chunkValues, max = 0) {
  if (!chunkValues.length) return max
  var newChunkValues = cuts.reduce((acc, cut) =>
    acc.concat(getCuttedChunkValues(cut, chunkValues)), [])

  var newMax = getMaxChunkValue(max, newChunkValues)

  return calcMaxValue(cuts, newChunkValues, newMax)
}

function getCuttedChunkValues (cut, chunkValues) {
  return chunkValues.reduce((acc, chunkValue) => {
    var newChunkValue = cutChunk(cut, chunkValue)
    if (newChunkValue) acc.push(newChunkValue)
    return acc
  }, [])
}

function cutChunk (cut, [chunk, value]) {
  if (chunk >= cut.size) return [chunk - cut.size, value + cut.value]
}

function getMaxChunkValue (max, chunkValues) {
  return chunkValues.reduce((acc, cur) =>
    Math.max(acc, cur[1] - cur[0]), max)
}

console.assert(cutStone([[23], data.diamond.cuts]) === 27)
